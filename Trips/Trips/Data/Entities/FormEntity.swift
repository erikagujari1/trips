//
//  FormEntity.swift
//  Trips
//
//  Created by AGUJARI Erik on 11/03/2020.
//  Copyright © 2020 ErikAgujari. All rights reserved.
//

struct FormData {
    let name: String
    let surname: String
    let email: String
    let phone: String
    let date: String
    let description: String
}

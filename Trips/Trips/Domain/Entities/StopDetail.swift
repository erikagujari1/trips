//
//  StopDetail.swift
//  Trips
//
//  Created by AGUJARI Erik on 06/03/2020.
//  Copyright © 2020 ErikAgujari. All rights reserved.
//
import Foundation

struct StopDetail {
    let userName: String
    let price: Double
    let stopTime: Date
    let paid: Bool
    let address: String
}
